var numArr = [];

 function addNumber(){
    addNumberValue = document.getElementById("txt-number").value*1;
    numArr.push(addNumberValue);
    document.getElementById("txt-number").value = ""
    document.getElementById("ket-qua").innerText = numArr;

    // Bài 1: Tổng số dương
    document.getElementById("testMot").addEventListener("click",function(){
        var tinhTong = 0;
        for (var i = 0; i < numArr.length; i++){
            if (numArr[i] > 0){
                tinhTong += numArr[i];
                document.getElementById("ket-qua1").innerHTML = `Tổng số dương: ${tinhTong}`
            } else {
                document.getElementById("ket-qua1").innerHTML = `Tổng số dương: ${tinhTong}` 
            }
        }
    })

    // Bài 2: Đếm số dương
    document.getElementById("testHai").addEventListener("click",function(){
        var count = 0;
        for (var i = 0; i < numArr.length; i++){
            if (numArr[i] > 0){
                count++
                document.getElementById("ket-qua2").innerHTML = `Số dương: ${count}` 
            }  else {
                document.getElementById("ket-qua2").innerHTML = `Số dương: ${count}` 
            }
         }
    })

    // Bài 3: Tìm số nhỏ nhất
    document.getElementById("testBa").addEventListener("click",function(){
        var minNumber = numArr[0]
        for (var i = 0; i < numArr.length; i++){ 
            if (minNumber > numArr[i]){
                minNumber = numArr[i] 
            }
            document.getElementById("ket-qua3").innerHTML = `Số nhỏ nhất: ${minNumber}`
        }
    })
 

    // Bài 4: Tìm số dương nhỏ nhất

    document.getElementById("testBon").addEventListener("click",function(){
        var positiveList = [];
        var minNumber = null;

        for (var i = 0; i < numArr.length; i++){ 
            if (numArr[i] > 0 ){
                positiveList.push(numArr[i])
            } 
        } 
        
        for (var i = 0; i <= positiveList.length; i++){
            minNumber = positiveList[0];
            if(positiveList.length !== 0 ){
                if (minNumber >= positiveList[i] ){
                    minNumber = positiveList[i]
                    document.getElementById("ket-qua4").innerHTML = `Số dương nhỏ nhất: ${minNumber}`
                }
            } else {
                document.getElementById("ket-qua4").innerHTML = ` Không có số dương trong mảng `
            } 
        } 
    })

    // Bài 5:  Tìm số chẵn cuối cùng 
    document.getElementById("testNam").addEventListener("click",function(){
        var lastEvenNumber = null;
        for (var i = 0; i < numArr.length; i++ ){
            if (numArr[i] % 2 === 0){
                lastEvenNumber = numArr[i];
                document.getElementById("ket-qua5").innerHTML = `Số chẵn cuối cùng: ${lastEvenNumber}`
            } else{
                lastEvenNumber = 0
                document.getElementById("ket-qua5").innerHTML = `Số chẵn cuối cùng: ${lastEvenNumber}`
            }
        } 
    })

    // Bài 6: Đổi chỗ 
    document.getElementById("testSau").addEventListener("click",function(){ 
        var viTriMot = document.getElementById("txt-vi-tri1").value*1;
        var viTriHai = document.getElementById("txt-vi-tri2").value*1;
        let temp = numArr[viTriMot];
        numArr[viTriMot] = numArr[viTriHai];
        numArr[viTriHai] = temp;
        document.getElementById("ket-qua6").innerHTML = `Mảng sau khi đổi: ${numArr}`
    })

    //  Bài 7: Sắp xếp tăng dần 
    document.getElementById("testBay").addEventListener("click",function(){
        var temp = null ;
        for (var i = 0; i < numArr.length; i++) {
            for (var j = i + 1; j < numArr.length; j++) {
              if (numArr[i] > numArr[j]) {
                temp = numArr[i];
                numArr[i] = numArr[j];
                numArr[j] = temp;
              }
            }
          } document.getElementById("ket-qua7").innerHTML = `Mảng sau khi sắp xếp: ${numArr}`
    })

    // Bài 8: Tìm số nguyên tố đầu tiên 
    document.getElementById("testTam").addEventListener("click",function(){
        
        function checkSNT(numb) {
            if (numb < 2)
               return 0;
            for (var i = 2; i < numb; i++)
               if (numb % i === 0)
                  return 0;
            return 1;
        }
         
        for (var i = 0; i < numArr.length; i++) {
            if (checkSNT(numArr[i]) === 1){
               document.getElementById("ket-qua8").innerHTML = `Số nguyên tố đầu tiên: ${numArr[i]}`
               break; 
            } else{
                document.getElementById("ket-qua8").innerHTML = `-1`
            }
        } 
    })

    //  Bài 10: So sánh số lượng số âm và dương 

    document.getElementById("testMuoi").addEventListener("click",function(){
        var countPositiveNumber = 0;
        var countNegativeNumber = 0;
        for (var i = 0; i < numArr.length; i++) {
            if (numArr[i] > 0){
                countPositiveNumber++;
            } else {
                countNegativeNumber++;
            }
        }
        if (countPositiveNumber > countNegativeNumber){
            document.getElementById("ket-qua10").innerHTML = `Số dương > Số âm`
        } else if (countPositiveNumber === countNegativeNumber){
            document.getElementById("ket-qua10").innerHTML = `Số dương = Số âm`
        } else {
            document.getElementById("ket-qua10").innerHTML = `Số dương < Số âm`
        }

    })

} 

// Bài 9: Đếm số nguyên 
var numArrT2 = [];
document.getElementById("themSoT2").addEventListener("click",function(){
    var countSN = 0;
    var addNumberValueT2 = document.getElementById("txt-them-sot2").value * 1;
    numArrT2.push(addNumberValueT2);
    document.getElementById("txt-them-sot2").value = ""
    document.getElementById("them-so-t2").innerText = numArrT2;

    document.getElementById("testChin").addEventListener("click",function(){
        for(var i = 0; i < numArrT2.length; i++){
            if(Number.isInteger(numArrT2[i]) === true){
                countSN++
                document.getElementById("ket-qua9").innerHTML = `Số nguyên: ${countSN}`
            } else{
                break;
            }
        }
    })
})





         

        
     



 

 
 
 
  






